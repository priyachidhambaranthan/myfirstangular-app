import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Iemployee } from '../emp';
import { environment } from '../../environments/environment';



@Injectable()

export class StudentService {
 baseUrl  = "http://localhost:3000/users";


  constructor(private http:HttpClient) { }

  public getstuddent():Observable<Iemployee[]>
  {
    return this.http.get<Iemployee[]>("http://localhost:3000/users");
  }

  public getById(id: any)
  {
    return this.http.get <Iemployee[]>(`${this.baseUrl}/${id}`);

  }

  public create(params:any)
  {
    return this.http.post("http://localhost:3000/users", params);
  }

  public update(id:number, params: any) {
    return this.http.put(`${this.baseUrl}/${id}`, params);
}
 delete(id: string) {
  
  return this.http.delete(`${this.baseUrl}/${id}`);
}
}