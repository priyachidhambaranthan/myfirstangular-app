import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router,ParamMap} from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public studentid:any;
  constructor(private activeroute:ActivatedRoute,private router:Router) { }

  ngOnInit() {  
 
    //let id:number= Number(this.activeroute.snapshot.paramMap.get('id'));

       this.activeroute.paramMap.subscribe((params:ParamMap)=>{

        let id:any = Number(params.get('id'));

        this.studentid =id;


       }); }

       goback()
       {
         this.router.navigate(['../'],{relativeTo: this.activeroute});
       }
     
    

}
