import { Component, OnInit } from '@angular/core';
import { StudentService } from 'src/app/services/student.service';
import { Iemployee } from 'src/app/emp';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  students:any;

  constructor(private _service:StudentService,
    private router:Router) { }

  ngOnInit(): void {

    this._service.getstuddent().subscribe(data =>this.students=data)
  }
  deleteUser(id:string)
  {
    const students = this.students.find((x: { id: string; }) => x.id === id)
    this._service.delete(id)
    .subscribe(() => this.students =this.students.filter((x: { id: string; }) =>x.id!==id));
    

}
}
