import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list.component';
import { AddEditComponent } from './add-edit.component';
import { LayoutComponent } from './layout/layout.component';



const routes: Routes = [
    {
      path: '',
      component:LayoutComponent,
      children: [
          {path:'',component:ListComponent},
          { path: 'add', component: AddEditComponent},
          {path: 'edit/:id', component: AddEditComponent}
    ]
}
];

  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

  export class UersRoutingModule{}