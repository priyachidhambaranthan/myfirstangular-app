import { createHostListener } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentService } from '../services/student.service';


@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.css']
})



export class AddEditComponent implements OnInit {

form!: FormGroup;
id!: number;
isAddMode!: boolean;
loading = false;
submitted = false;
 
  constructor(
    private fb:FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private service:StudentService ){}
 
 
  ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        this.isAddMode = !this.id;

         this.form = this.fb.group({
          firstname: ['',Validators.required],
          lastname:['',Validators.required],
          username:['',Validators.required],
          email:['',[Validators.required,Validators.email]]
        } );   
        
        if (!this.isAddMode) {
          this.service.getById(this.id)
              .subscribe(x => this.form.patchValue(x));
        }
      }
      onSubmit(){
      this.loading=true;

      if(this.isAddMode)
      {
        this.currentuser();
      }
      else{
         this.updateuser();
      }
      
    }
  updateuser() {
    this.service.update(this.id, this.form.value)
    .subscribe({
      next: () => {
          this.router.navigate(['../../'], { relativeTo: this.route });
      },
      error: error => {
  
          this.loading = false;
      }
  });
}
  currentuser() {
    console.log(this.form.value); 
    this.service.create(this.form.value).subscribe({
     next: () => {
       this.router.navigate(['../'],{relativeTo:this.route});},
       error: error => console.log("error")
    });
  }
  }
