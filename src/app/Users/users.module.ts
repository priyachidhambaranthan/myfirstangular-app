import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list.component';
import { AddEditComponent } from './add-edit.component';
import { UersRoutingModule } from './users-routingmodule';
import { LayoutComponent } from './layout/layout.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    UersRoutingModule,
    ReactiveFormsModule
    
  ],
  declarations: [ListComponent,AddEditComponent, LayoutComponent]
})
export class Usersmodule{}