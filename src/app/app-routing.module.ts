import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

 const routes: Routes = [
   { path: '', component: DashboardComponent },
   {
     path:'users',
    loadChildren: () => import('./Users/users.module').then(m => m.Usersmodule)},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
    
 ]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule,]
})
export class AppRoutingModule { }
